test("01", () => {
  const conf = require("../../index")({
    foo: "bar1",
  })
    .js()
    .exec();
  expect(conf).toMatchInlineSnapshot(`
    Object {
      "foo": "bar2",
      "initialize": [Function],
      "toCamelCase": [Function],
    }
  `);
});
