test("01", () => {
  process.env.conf = `FOO_A: bar3
foo: bar2
arr:
  - a
  - b`;
  const conf = require("../../index")({
    foo: "bar1",
  })
    .loadEnv()
    .exec();
  expect(conf).toMatchInlineSnapshot(`
    Object {
      "FOO_A": "bar3",
      "arr": Array [
        "a",
        "b",
      ],
      "foo": "bar2",
      "initialize": [Function],
      "toCamelCase": [Function],
    }
  `);
});
