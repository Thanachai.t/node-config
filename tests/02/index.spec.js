test("02", () => {
  const conf = require("../../index")({
    fooA: "bar1",
  })
    .js()
    .exec();
  expect(conf).toMatchInlineSnapshot(`
    Object {
      "fooA": "bar2",
      "initialize": [Function],
      "toCamelCase": [Function],
    }
  `);
});
