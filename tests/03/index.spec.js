test("03", () => {
  const conf = require("../../index")({
    fooA: "bar1",
  })
    .env()
    .exec();
  expect(conf.fooA).toMatchInlineSnapshot(`"bar3"`);
  // expect(conf).toMatchInlineSnapshot(`
  //   Object {
  //     "foo_a": "bar2",
  //     "initialize": [Function],
  //     "toCamelCase": [Function],
  //   }
  // `);
});
