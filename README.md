Please visit the new location at:

[https://gitlab.com/cat-config/cat-config](https://gitlab.com/cat-config/cat-config)


# Install
```bash
yarn add git+https://public:_-Jqzy5yoFcNYrH7SeSX@www.gitlab.com/thanachai.t/node-config.git
```

# Example
```javascript
// config.js
let config = {
  PORT: 80
}

config = require('config')(config)
  .env()
  .js()
  .yaml()
  .argv()
  .mongodb('mongodb://localhost:27017', 'config', 'config')
  .exec()

module.exports = config

// use
const CONF = require('./config')
console.log(CONF)
/**
 * {
 *    PORT: 80
 * }
 */

```


```javascript
await CONF.$set('foo', 'bar')
```

