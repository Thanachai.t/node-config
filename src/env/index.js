const path = require('path')
const fs = require('fs')
require('../utils/checkPkg')(['dotenv'])
const debug = require('debug')('config')
const merge = require('../utils/deepmerge')
const dotenv = require('dotenv')
const _ = require('lodash')
const ChangeCase = require('change-case')

let conf

module.exports = function (c = {}, ops = {}) {
  if (!conf) {
    function log(str) {
      debug(str)
      ops.log && ops.log(str)
    }

    let mainPath = ops.mainPath || _.get(process, 'mainModule.path') || _.get(require, 'main.path') || ''

    // load config.js file
    const configFile = path.resolve(mainPath, '.env')
    if (fs.existsSync(configFile)) {
      log('load config: ' + configFile)
      dotenv.config({
        path: configFile
      })
    }

    // load config.dev.js file
    if (process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'production') {
      const configDevFile = path.resolve(mainPath, 'dev.env')
      if (fs.existsSync(configDevFile)) {
        log('load config: ' + configDevFile)
        require('dotenv').config({
          path: configDevFile
        })
      }
    }

    log('load config: process.env')
    conf = merge(conf || {}, process.env, x => ChangeCase[conf.case || 'camelCase'](x))
  }

  return merge(c, conf || {}, x => ChangeCase[conf.case || 'camelCase'](x))
}
