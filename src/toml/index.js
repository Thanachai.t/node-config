const fs = require('fs')
const path = require('path')

require('../utils/checkPkg')(['toml'])
const merge = require('../utils/deepmerge')
const debug = require('debug')('config')
const toml = require('toml')
const _ = require('lodash')

let conf

module.exports = function (c = {}, ops) {
  if (!conf) {
    function log(str) {
      debug(str)
      ops.log && ops.log(str)
    }

    let mainPath = ops.mainPath || _.get(process, 'mainModule.path') || _.get(require, 'main.path')

    const configFile = path.resolve(mainPath, `${ops.fileName}.toml`)

    // load config.yaml file
    if (fs.existsSync(configFile)) {
      log('load config: ' + configFile)
      conf = merge(toml.load(configFile) || {});
    }

    // load config.dev.yaml file
    if (process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'production') {
      const configDevFile = path.resolve(mainPath, `${ops.fileName}.dev.toml`)
      if (fs.existsSync(configDevFile)) {
        log('load config: ' + configDevFile)
        conf = merge(conf || {}, toml.parse(configDevFile) || {});
      }
    }
  }

  return merge(c, conf || {})
}