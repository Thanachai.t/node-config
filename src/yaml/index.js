const fs = require('fs')
const path = require('path')

require('../utils/checkPkg')(['yamljs'])
const merge = require('../utils/deepmerge')
const debug = require('debug')('config')
const YAML = require('yamljs')
const _ = require('lodash')
const { getMainPath } = require('../utils')

let conf

module.exports = function (c = {}, ops) {
  if (!conf) {
    function log(str) {
      debug(str)
      ops.log && ops.log(str)
    }
    let mainPath = ops.mainPath || getMainPath()

    if (process.env.CONF) {
      conf = merge(conf || {}, YAML.parse(process.env.CONF) || {});
    }

    const configFile = path.resolve(mainPath, `${ops.fileName}.yaml`)

    // load config.yaml file
    if (fs.existsSync(configFile)) {
      log('load config: ' + configFile)
      conf = merge(conf || {}, YAML.load(configFile) || {});
    }

    // load config.dev.yaml file
    if (process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'production') {
      const configDevFile = path.resolve(mainPath, '../', `${ops.fileName}.dev.yaml`)
      if (fs.existsSync(configDevFile)) {
        log('load config: ' + configDevFile)
        conf = merge(conf || {}, YAML.load(configDevFile) || {});
      }
    }

  }

  return merge(c, conf || {})
}