require('../utils/checkPkg')(['ioredis', 'yargs'])

const argv = require('yargs').argv

const Redis = require('ioredis')

const uri = argv._[0] || "redis://localhost:6379";
const redis = new Redis(uri);

redis.send_command(argv.c || 'GET', argv.k || 'conf', function (err, result) {
  if (err) throw err;

  process.stdout.write(result || {})
  process.exit(0)
})
