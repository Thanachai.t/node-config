const shelljs = require("shelljs");

test("redis 1", () => {
  shelljs.exec = jest.fn(() => ({
    code: 0,
    stdout: "foo: bar2",
  }));

  const conf = require("../../index")({
    foo: "bar1",
  })
    .redis()
    .exec();
  expect(conf).toMatchInlineSnapshot(`
    Object {
      "foo": "bar2",
      "initialize": [Function],
      "toCamelCase": [Function],
    }
  `);
});
