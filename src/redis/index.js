require('../utils/checkPkg')(['shelljs', 'lodash'])
const shell = require('shelljs')
const merge = require('../utils/deepmerge')
const debug = require('debug')('config')
const _ = require('lodash')
let conf

module.exports = function (c = {}, uri = 'redis://localhost', { key, type, cmd } = {}, ops = {}) {
  if (!conf) {
    function log(str) {
      debug(str)
      ops.log && ops.log(str)
    }

    log('load config: ' + uri.replace(/\/\/([^:]*):([^@]+)@/, '//$1:****@'))

    const execStr = `node ${__dirname}/script.js ${uri} -k ${key || 'conf'} -t ${type || 'yaml'} -c ${cmd || 'GET'}`
    let childProcess = shell.exec(execStr, { async: false, silent: true })

    if (childProcess.code !== 0)
      throw new Error(childProcess.stderr)

    let c = {}
    if (type == 'json') {
      c = JSON.parse(childProcess.stdout)
    } else {
      require('../utils/checkPkg')(['yamljs'])
      const YAML = require('yamljs')
      c = YAML.parse(childProcess.stdout) || {}
    }

    conf = merge(conf || {}, c)
  }

  c = merge(c, conf || {})

  return c
}