const fs = require('fs')
const path = require('path')

require('../utils/checkPkg')(['yargs'])
const merge = require('../utils/deepmerge')
const debug = require('debug')('config')
const argv = require('yargs').argv

let conf

module.exports = function (
  c = {}, {
    capitalization = 'constantCase'
  } = {},
  ops = {}
) {
  if (!conf && Object.keys(argv).filter(k => k !== '_' && k !== '$0').length > 0) {
    debug('load config: argv')
    ops.log && ops.log('load config: argv')
    conf = merge(conf || {}, argv)
  }

  return merge(c, conf || {})
}