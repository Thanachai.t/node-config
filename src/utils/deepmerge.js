require('./checkPkg')(['debug', 'change-case'])
const _ = require('lodash')

function re(o1, o2, o22 = {}, s = '', replacer) {
  if (o2 !== null && typeof o2 === 'object') {
    if (Array.isArray(o2)) {
      let ss = s.split(/\.|__/gm).map(replacer).join('.')
      _.set(o1, ss, _.get(o22, s))
    } else {
      for (let i in o2) {
        re(o1, o2[i], o22, s.length > 0 ? s + '.' + i : i, replacer)
      }
    }
  } else {
    let ss = s.split(/\.|__/gm).map(replacer).join('.')
    // if (_.has(o1, ss))
    _.set(o1, ss, _.get(o22, s))
  }
}

module.exports = function (o1 = {}, o2 = {}, replacer = x => x) {
  if (_.isEmpty(o1)) {
    return Object.assign({}, o2)
  }

  o1 = Object.assign(o1)

  re(o1, o2, o2, '', replacer)

  return o1
}