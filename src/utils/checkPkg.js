const fs = require('fs')
const path = require('path')
const shelljs = require('shelljs')
const _ = require('lodash')
const pkg = []
const debug = require('debug')('config')

module.exports = function (arr = []) {
  let mainPath
  for (let p of _.get(process, 'mainModule.paths', process.cwd())) {
    if (fs.existsSync(p)) {
      mainPath = path.resolve(p)
    }
  }

  if (mainPath === '/')
    mainPath = path.resolve(__dirname, '../../node_modules')
  debug('mainPath %o', mainPath)

  const pkgList = []
  for (let a of arr) {
    if (pkg.indexOf(a) === -1 && !fs.existsSync(path.resolve(mainPath, a))) pkgList.push(a)
  }

  if (pkgList.length > 0) {
    console.log(shelljs.which('yarn'));
    if (shelljs.which('yarn')) {
      console.log(`yarn add ` + pkgList.join(' '))

      shelljs.exec(`yarn add ` + pkgList.join(' '), {
        async: false,
        silent: true
      })
      console.log(shelljs.which('yarn'));
    } else {
      console.log(`npm i -S ` + pkgList.join(' '))

      shelljs.exec(`npm i -S ` + pkgList.join(' '), {
        async: false,
        silent: true
      })
    }

    pkg.push(...pkgList)
  }
}