const fs = require('fs')
const path = require('path')
const _ = require('lodash')
let mainPath

module.exports = {
  getMainPath() {
    if (mainPath) return mainPath

    mainPath = _.get(process, 'mainModule.path') || _.get(require, 'main.path')
    while (!fs.existsSync(path.join(mainPath, 'node_modules'))) {
      mainPath = path.join(mainPath, '../')
    }
    return mainPath
  }
}
