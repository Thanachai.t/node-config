const fs = require('fs')
const path = require('path')

const merge = require('../utils/deepmerge')
const debug = require('debug')('config')
const _ = require('lodash')

let conf

module.exports = function (c = {}, ops) {
  if (!conf) {
    function log(str) {
      debug(str)
      ops.log && ops.log(str)
    }

    let mainPath = ops.mainPath || _.get(process, 'mainModule.path') || _.get(require, 'main.path')

    const configFile = path.resolve(mainPath, `${ops.fileName}.json`)

    // load config.js file
    if (fs.existsSync(configFile)) {
      log('load config: ' + configFile)
      conf = require(configFile)
    }

    // load config.dev.js file
    if (process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'production') {
      const configDevFile = path.resolve(mainPath, `${ops.fileName}.dev.json`)
      if (fs.existsSync(configDevFile)) {
        log('load config: ' + configDevFile)
        conf = merge(conf || {}, require(configDevFile));
      }
    }

  }

  return merge(c, conf || {})
}