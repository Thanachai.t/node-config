const merge = require('./utils/deepmerge')
const axios = require('axios')
const _ = require('lodash')

function convertObject(ob, c = 'camel') {
  let r = {}
  if (ob !== null && ob !== undefined && typeof ob === 'object') {
    for (let k of Object.keys(ob)) {
      if (typeof ob[k] === 'object') {
        r[_.camelCase(k)] = convertObject(ob[k], c)
      } else {
        if (c == 'camel') {
          r[_.camelCase(k)] = ob[k]
        } else {
          r[k] = ob[k]
        }
      }
    }

    return r
  } else {
    return ob
  }
}

let cf = {
  config: {
  },
  _options: {},
  _arr: [],
  argv(config) {
    this.config = config || this.config
    this.config = require('./argv')(this.config, this._options)
    return this
  },
  env(config) {
    this.config = config || this.config
    this.config = require('./env')(this.config, this._options)
    return this
  },
  js(config) {
    this.config = config || this.config
    this.config = require('./js')(this.config, this._options)
    return this
  },
  json(config) {
    this.config = config || this.config
    this.config = require('./json')(this.config, this._options)
    return this
  },
  yaml(config, ops) {
    this.config = config || this.config
    this.config = require('./yaml')(this.config, Object.assign(this._options, ops))
    return this
  },
  toml(config) {
    this.config = config || this.config
    this.config = require('./toml')(this.config, this._options)
    return this
  },
  url(url, { contentType } = {}) {
    this.config = require('./url')(url, this.config, this._options, { contentType })
    return this
  },
  loadEnv(envName = 'conf') {
    require('./utils/checkPkg')(['yamljs'])
    const merge = require('./utils/deepmerge')
    const YAML = require('yamljs')
    merge(this.config, YAML.parse(process.env[envName] || process.env[envName.toUpperCase()] || ''))
    return this
  },
  mongodb(...args) {
    this.config = require('./mongo')(this.config, ...args, this._options)
    return this
  },
  redis(uri = 'redis://localhost:6379', ...args) {
    this.config = require('./redis')(this.config, uri, ...args, this._options)
    return this
  },
  exec() {
    this.config.initialize = async function () {
      for (let c of cf._arr) {
        cf.config = merge(cf.config, await c)
      }
    }

    return this.config
  }
}

module.exports = function (config = {}, ops = {}) {
  cf.config = config
  cf._options = Object.assign({
    fileName: 'conf'
  }, ops)

  if (process.env.CONFIG) {
    cf._options.fileName = `conf.${process.env.CONFIG}`
  } else {
    cf._options.fileName = 'conf'
  }

  cf.config.toCamelCase = function (key) {
    if (key) {
      return convertObject(_.get(this, key))
    } else {
      return convertObject(this)
    }
  }
  return cf
}
