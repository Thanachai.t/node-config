require('../utils/checkPkg')(['shelljs', 'lodash', 'mongodb'])
const shell = require('shelljs')
const merge = require('../utils/deepmerge')
const debug = require('debug')('config')
const _ = require('lodash')
let conf

module.exports = function (c = {}, uri, dbName, collection, ops = {}) {
  if (!conf) {
    function log(str) {
      debug(str)
      ops.log && ops.log(str)
    }

    log('load config: ' + uri.replace(/\/\/([^:]+):([^@]+)@/, '//$1:****@'))

    let childProcess = shell.exec(`node ${__dirname}/script.js ${uri} ${dbName} ${collection}`, { async: false, silent: true })

    if (childProcess.code !== 0)
      throw new Error(childProcess.stderr)

    const c = JSON.parse(childProcess.stdout)

    conf = merge(conf || {}, c)
  }

  c = merge(c, conf || {})

  c.$set = async function (key, value) {
    _.set(c, key, value)

    const MongoClient = require('mongodb').MongoClient;

    MongoClient.connect(uri, async function (err, db) {
      if (err) throw err;
      const dbo = db.db(dbName);

      const Config = dbo.collection(collection)
      await Config.findOneAndUpdate(
        {},
        {
          $set: {
            [key]: value
          }
        },
        {
          upsert: true
        })

      db.close()
    });
  }

  return c
}