require('../utils/checkPkg')(['mongodb', 'yargs'])

const argv = require('yargs').argv

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb://localhost:27017";

MongoClient.connect(argv._[0] || uri, async function (err, db) {
  if (err) throw err;
  const dbo = db.db(argv._[1] || 'config');
  try {
    const System = dbo.collection(argv._[2] || "config")
    const system = await System.findOne()

    if (!system) await System.insert({})

    process.stdout.write(JSON.stringify(system || {}))

  } catch (err) {
    console.error(err)
    process.exit(1)
  }
  db.close()
})