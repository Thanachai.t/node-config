require('../utils/checkPkg')(['axios', 'shelljs'])
const debug = require('debug')('config')
const axios = require('axios')
const shell = require('shelljs')
const merge = require('../utils/deepmerge')

let conf

module.exports = function (url, c = {}, ops, { contentType } = {}) {
  if (!conf) {
    debug('load config: ' + url)

    let childProcess = shell.exec(`node ${__dirname}/script.js get ${url}`, { async: false, silent: true })

    if (childProcess.code !== 0)
      throw new Error(childProcess.stderr)

    const c = JSON.parse(childProcess.stdout)

    if (contentType == 'yaml' || contentType == 'yml') {
      const YAML = require('yamljs')
      conf = merge(conf || {}, YAML.parse(c.body))
    } else {
      conf = merge(conf || {}, c.body)
    }
  }

  return merge(c, conf || {})
}