(async () => {
  require('../utils/checkPkg')(['yargs', 'axios'])
  const axios = require('axios')

  const argv = require('yargs').argv

  await axios[argv._[0]](argv._[1])
    .then(r => {
      process.stdout.write(JSON.stringify({ headers: r.headers, body: r.data }))
    })
    .catch(err => {
      process.stderr.write(err)
    })
})()
